<?php

    $message_sent = false;

    if (isset($_POST['email']) && $_POST['email'] != "" && isset($_POST['postal']) && isset($_POST['phone']) && isset($_POST['title'])) {

        if (isset($_POST['addressline']) && isset($_POST['city']) && isset($_POST['country']) && isset($_POST['profession']) && isset($_POST['department']) && isset($_POST['request'])) {

            if (isset($_POST['onlinestore']) || isset($_POST['searchingonline']) || isset($_POST['socialmedia']) || isset($_POST['otherinput'])) {

                if (isset($_POST['privacypolicy'])) {

                    $Title = $_POST['title'];
                    $Name = $_POST['name'];
                    $Email = $_POST['email'];
                    $Phone = $_POST['phone'];
                    $AddressLine = $_POST['addressline'];
                    $City = $_POST['city'];
                    $Region = $_POST['region'];
                    $Postal = $_POST['postal'];
                    $Country = $_POST['country'];
                    $Profession = $_POST['profession'];
                    $Department = $_POST['department'];
                    $Request = $_POST['request'];
                    $OnlineStore = $_POST['onlinestore'];
                    $SearchingOnline = $_POST['searchingonline'];
                    $SocialMedia = $_POST['socialmedia'];
                    $OtherInput = $_POST['otherinput'];
                    $PrivacyPolicy = $_POST['privacypolicy'];

                    $to = "info@medicinevirtual.com";
                    $body = "";

                    $body .= "Title: ".$Title. "\r\n";
                    $body .= "Name: ".$Name. "\r\n";
                    $body .= "Email: ".$Email. "\r\n";
                    $body .= "Phone: ".$Phone. "\r\n\r\n";
                    $body .= "Address Line: ".$AddressLine. "\r\n";
                    $body .= "City: ".$City. "\r\n";
                    $body .= "State / Province / Region: ".$Region. "\r\n";
                    $body .= "Postal / Zip Code: ".$Postal. "\r\n";
                    $body .= "Country: ".$Country. "\r\n\r\n";
                    $body .= "Profession: ".$Profession. "\r\n";
                    $body .= "Institution and Department: ".$Department. "\r\n\r\n";
                    $body .= "Request: ".$Request. "\r\n\r\n";
                    $body .= "I read and agree with Privacy Policy.: ".$PrivacyPolicy. "\r\n\r\n";
                    $body .= "How did you know about Human Anatomy VR?\r\n";
                    $body .= "From an online store: ".$OnlineStore. "\r\n";
                    $body .= "Searching online: ".$SearchingOnline. "\r\n";
                    $body .= "From social media: ".$SocialMedia. "\r\n";
                    $body .= "Other: ".$OtherInput. "\r\n";

                    mail($to, "Enterprise Edition - Contact form", $body);

                    $message_sent = true;
                }
            }
        }
    }
?>

<!doctype html>
<html class="no-js" lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

<head>
  <meta charset="utf-8">
  <title>Virtual Medicine</title>
  <meta name="description" content="Inovácia vzdelávania zdravotníckych pracovníkov a študentov">
  <meta name="twitter:description" content="Inovácia vzdelávania zdravotníckych pracovníkov a študentov">
  <meta name="keywords" content="VR, Virtual Reality, Human Anatomy, DICOM, CT, MRI">
  <meta name="author" content="Design: Michal Tkáč, Development: Ondrej Malota">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="Virtual Medicine">
  <meta property="og:description" content="Inovácia vzdelávania zdravotníckych pracovníkov a študentov">
  <meta property="og:type" content="website">
  <meta property="og:url" content="https://medicinevirtual.com">
  <meta property="og:image" content="https://medicinevirtual.com/img/cover_img.png">

  <link rel="apple-touch-icon" href="icon.png">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#FFFFFF">
</head>

<body>

  <div class="bg_blur">
  </div>

  <header>
    <div class="burger_menu_bg">
      <div class="multilanguage-burger">
        <img src="img/svk-flag.png" style="opacity: 1; cursor: default">
        <img src="img/uk-flag.png" onclick="location.href='../contact-form.php';">
      </div>
      <ul>
        <li class=""><a href="xrexperience.html"><img src="img/burger_menu_icon_xr_experience.svg"/>Produkty</a></li>
        <li class=""><a href="student.html"><img src="img/burger_menu_icon_student.svg" style="width: 35px;"/>Študent</a></li>
        <li class=""><a href="professional.html"><img src="img/burger_menu_icon_professional.svg" style="width: 35px;"/>Profesionál</a></li>
        <li class=""><a href="institution.html"><img src="img/burger_menu_icon_institution.svg"/>Inštitúcia</a></li>
        <li class=""><a href="contact.html"><img src="img/burger_menu_icon_contact_us.svg"/>O nás</a></li>
      </ul>
      <button class="try" onclick="location.href='contact-form.php';" style="border-radius: 20px;">KONTAKTUJ NÁS</button>
      <div class="burger_social_bar">
        <img src="img/social-facebook.svg" alt="FACEBOOK" class="social_ico" onclick="window.open('https://www.facebook.com/VRMedicine/')"/>
        <img src="img/social-instagram.svg" alt="INSTAGRAM" class="social_ico" onclick="window.open('https://www.instagram.com/virtual_medicine/?hl=en')"/>
        <img src="img/social-linkedin.svg" alt="LINKEDIN" class="social_ico" onclick="window.open('https://www.linkedin.com/company/virtual-medicine-vr-ar')"/>
        <img src="img/social-youtube.svg" alt="YOUTUBE" class="social_ico" onclick="window.open('https://www.youtube.com/channel/UChvqcOhNLxxWBc0FACQeXHg')"/>
      </div>
    </div>
    <div class="nav_container nav_burger_container">
      <nav>
        <span class="nav_logo" onclick="location.href='index.html';">
        <img src="img/nav-logo.png" alt="VIRTUAL MEDICINE"/>
        </span>
  <!-- burger navbar -->
          <div class="burger_navbar_container">
            <div class="burger_navbar_item"></div>
          </div>

  <!-- burger navbar end -->
        <ul>
          <li class="nav_item_bg"></li>
          <li class="nav_item"><a href="xrexperience.html">Produkty</a></li>
          <li class="nav_item"><a href="student.html">Študent</a></li>
          <li class="nav_item"><a href="professional.html">Profesionál</a></li>
          <li class="nav_item"><a href="institution.html">Inštitúcia</a></li>
          <li class="nav_item"><a href="contact.html">O nás</a></li>
        </ul>
        <button class="try" onclick="location.href='contact-form.php';">KONTAKTUJ NÁS</button>
        <div class="multilanguage responsive_hidden">
          <img src="img/svk-flag.png" style="opacity: 1; cursor: default">
          <img src="img/uk-flag.png" onclick="location.href='../contact-form.php';">
        </div>
      </nav>
    </div>
  </header>
    <?php
    if($message_sent):
    ?>
        <script>alert("You message has been sent!");</script>
    <?php
    endif;
    ?>
  <section class="contact_form">
    <h3>Neváhajte nás kontaktovať</h3><br><br>
    <p>Vyplňte prosím nasledujúci formulár a my vás budeme čoskoro kontaktovať<br><br>
    Vaše súkromie rešpektujeme na 100 %, preto<br>informácie, ktoré nám poskytnete, zostanú prísne dôverné.</p>
    <br><br>
    <form action="contact-form.php" method="POST" class="form" id="form">
      <div class="form_item_group form_item_group_mobile" style="align-items: flex-end;">
        <div class="form_item input_text title-width" style="background-color: white; display: flex; flex-direction: row;">
            <select name="title" class="valid_text">
            <option value="" selected disabled hidden>Titul</option>
            <option value="Mr">Mr.</option>
            <option value="Mrs">Mrs.</option>
            <option value="Mrs">Dr.</option>
            <option value="Mrs">Prof.</option>
            <option value="Mrs">Ms.</option>
          </select>
            <img src="img/down-arrow.svg" class="country_img"/>
        </div>
        <div class="form_item name-width">
          <label for="name" class="label_text">Meno <span style="color: darkred;">&nbsp*</span></label>
          <input name="name" type="text" placeholder="Celé meno" class="input_text valid_text">
        </div>
        <div class="form_item email-width">
          <label for="email" class="label_text">Email <span style="color: darkred;">&nbsp*</span></label>
          <input name="email" type="email" placeholder="Váš email" class="input_text valid_email">
        </div>
      </div>
      <div class="form_item_group form_item_group_mobile">
          <div class="form_item form_item_addressline">
            <label for="addressline" class="label_text">Adresa <span style="color: darkred;">&nbsp*</span></label>
            <input name="addressline" type="text" placeholder="Adresný riadok" class="input_text valid_text">
          </div>
          <div class="form_item form_item_addressline">
            <label for="phone" class="label_text">Telefón <span style="color: darkred;">&nbsp*</span></label>
            <input name="phone" type="text" placeholder="Telefónne číslo" class="input_text" id="phone">
          </div>
      </div>
      <div class="form_item_group form_item_group_mobile">
        <input name="city" type="text" placeholder="City" class="input_text valid_text">
        <input name="region" type="text" placeholder="Štát / Provincia / Región" class="input_text valid_text">
      </div>
      <div class="form_item_group form_item_group_mobile">
        <input name="postal" type="text" placeholder="Poštové smerovacie číslo" class="input_text valid_post_code">
        <div class="form_item input_text" style="background-color: white; display: flex; flex-direction: row;">
            <select name="country" class="valid_text">
            <option value="" selected disabled hidden>Krajina</option>
            <option value="Afghanistan">Afghanistan</option>
            <option value="Åland Islands">Åland Islands</option>
            <option value="Albania">Albania</option>
            <option value="Algeria">Algeria</option>
            <option value="American Samoa">American Samoa</option>
            <option value="Andorra">Andorra</option>
            <option value="Angola">Angola</option>
            <option value="Anguilla">Anguilla</option>
            <option value="Antarctica">Antarctica</option>
            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
            <option value="Argentina">Argentina</option>
            <option value="Armenia">Armenia</option>
            <option value="Aruba">Aruba</option>
            <option value="Australia">Australia</option>
            <option value="Austria">Austria</option>
            <option value="Azerbaijan">Azerbaijan</option>
            <option value="Bahamas">Bahamas</option>
            <option value="Bahrain">Bahrain</option>
            <option value="Bangladesh">Bangladesh</option>
            <option value="Barbados">Barbados</option>
            <option value="Belarus">Belarus</option>
            <option value="Belgium">Belgium</option>
            <option value="Belize">Belize</option>
            <option value="Benin">Benin</option>
            <option value="Bermuda">Bermuda</option>
            <option value="Bhutan">Bhutan</option>
            <option value="Bolivia">Bolivia</option>
            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
            <option value="Botswana">Botswana</option>
            <option value="Bouvet Island">Bouvet Island</option>
            <option value="Brazil">Brazil</option>
            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
            <option value="Brunei Darussalam">Brunei Darussalam</option>
            <option value="Bulgaria">Bulgaria</option>
            <option value="Burkina Faso">Burkina Faso</option>
            <option value="Burundi">Burundi</option>
            <option value="Cambodia">Cambodia</option>
            <option value="Cameroon">Cameroon</option>
            <option value="Canada">Canada</option>
            <option value="Cape Verde">Cape Verde</option>
            <option value="Cayman Islands">Cayman Islands</option>
            <option value="Central African Republic">Central African Republic</option>
            <option value="Chad">Chad</option>
            <option value="Chile">Chile</option>
            <option value="China">China</option>
            <option value="Christmas Island">Christmas Island</option>
            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
            <option value="Colombia">Colombia</option>
            <option value="Comoros">Comoros</option>
            <option value="Congo">Congo</option>
            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
            <option value="Cook Islands">Cook Islands</option>
            <option value="Costa Rica">Costa Rica</option>
            <option value="Cote D'ivoire">Cote D'ivoire</option>
            <option value="Croatia">Croatia</option>
            <option value="Cuba">Cuba</option>
            <option value="Cyprus">Cyprus</option>
            <option value="Czech Republic">Czech Republic</option>
            <option value="Denmark">Denmark</option>
            <option value="Djibouti">Djibouti</option>
            <option value="Dominica">Dominica</option>
            <option value="Dominican Republic">Dominican Republic</option>
            <option value="Ecuador">Ecuador</option>
            <option value="Egypt">Egypt</option>
            <option value="El Salvador">El Salvador</option>
            <option value="Equatorial Guinea">Equatorial Guinea</option>
            <option value="Eritrea">Eritrea</option>
            <option value="Estonia">Estonia</option>
            <option value="Ethiopia">Ethiopia</option>
            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
            <option value="Faroe Islands">Faroe Islands</option>
            <option value="Fiji">Fiji</option>
            <option value="Finland">Finland</option>
            <option value="France">France</option>
            <option value="French Guiana">French Guiana</option>
            <option value="French Polynesia">French Polynesia</option>
            <option value="French Southern Territories">French Southern Territories</option>
            <option value="Gabon">Gabon</option>
            <option value="Gambia">Gambia</option>
            <option value="Georgia">Georgia</option>
            <option value="Germany">Germany</option>
            <option value="Ghana">Ghana</option>
            <option value="Gibraltar">Gibraltar</option>
            <option value="Greece">Greece</option>
            <option value="Greenland">Greenland</option>
            <option value="Grenada">Grenada</option>
            <option value="Guadeloupe">Guadeloupe</option>
            <option value="Guam">Guam</option>
            <option value="Guatemala">Guatemala</option>
            <option value="Guernsey">Guernsey</option>
            <option value="Guinea">Guinea</option>
            <option value="Guinea-bissau">Guinea-bissau</option>
            <option value="Guyana">Guyana</option>
            <option value="Haiti">Haiti</option>
            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
            <option value="Honduras">Honduras</option>
            <option value="Hong Kong">Hong Kong</option>
            <option value="Hungary">Hungary</option>
            <option value="Iceland">Iceland</option>
            <option value="India">India</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
            <option value="Iraq">Iraq</option>
            <option value="Ireland">Ireland</option>
            <option value="Isle of Man">Isle of Man</option>
            <option value="Israel">Israel</option>
            <option value="Italy">Italy</option>
            <option value="Jamaica">Jamaica</option>
            <option value="Japan">Japan</option>
            <option value="Jersey">Jersey</option>
            <option value="Jordan">Jordan</option>
            <option value="Kazakhstan">Kazakhstan</option>
            <option value="Kenya">Kenya</option>
            <option value="Kiribati">Kiribati</option>
            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
            <option value="Korea, Republic of">Korea, Republic of</option>
            <option value="Kuwait">Kuwait</option>
            <option value="Kyrgyzstan">Kyrgyzstan</option>
            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
            <option value="Latvia">Latvia</option>
            <option value="Lebanon">Lebanon</option>
            <option value="Lesotho">Lesotho</option>
            <option value="Liberia">Liberia</option>
            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
            <option value="Liechtenstein">Liechtenstein</option>
            <option value="Lithuania">Lithuania</option>
            <option value="Luxembourg">Luxembourg</option>
            <option value="Macao">Macao</option>
            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
            <option value="Madagascar">Madagascar</option>
            <option value="Malawi">Malawi</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Maldives">Maldives</option>
            <option value="Mali">Mali</option>
            <option value="Malta">Malta</option>
            <option value="Marshall Islands">Marshall Islands</option>
            <option value="Martinique">Martinique</option>
            <option value="Mauritania">Mauritania</option>
            <option value="Mauritius">Mauritius</option>
            <option value="Mayotte">Mayotte</option>
            <option value="Mexico">Mexico</option>
            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
            <option value="Moldova, Republic of">Moldova, Republic of</option>
            <option value="Monaco">Monaco</option>
            <option value="Mongolia">Mongolia</option>
            <option value="Montenegro">Montenegro</option>
            <option value="Montserrat">Montserrat</option>
            <option value="Morocco">Morocco</option>
            <option value="Mozambique">Mozambique</option>
            <option value="Myanmar">Myanmar</option>
            <option value="Namibia">Namibia</option>
            <option value="Nauru">Nauru</option>
            <option value="Nepal">Nepal</option>
            <option value="Netherlands">Netherlands</option>
            <option value="Netherlands Antilles">Netherlands Antilles</option>
            <option value="New Caledonia">New Caledonia</option>
            <option value="New Zealand">New Zealand</option>
            <option value="Nicaragua">Nicaragua</option>
            <option value="Niger">Niger</option>
            <option value="Nigeria">Nigeria</option>
            <option value="Niue">Niue</option>
            <option value="Norfolk Island">Norfolk Island</option>
            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
            <option value="Norway">Norway</option>
            <option value="Oman">Oman</option>
            <option value="Pakistan">Pakistan</option>
            <option value="Palau">Palau</option>
            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
            <option value="Panama">Panama</option>
            <option value="Papua New Guinea">Papua New Guinea</option>
            <option value="Paraguay">Paraguay</option>
            <option value="Peru">Peru</option>
            <option value="Philippines">Philippines</option>
            <option value="Pitcairn">Pitcairn</option>
            <option value="Poland">Poland</option>
            <option value="Portugal">Portugal</option>
            <option value="Puerto Rico">Puerto Rico</option>
            <option value="Qatar">Qatar</option>
            <option value="Reunion">Reunion</option>
            <option value="Romania">Romania</option>
            <option value="Russian Federation">Russian Federation</option>
            <option value="Rwanda">Rwanda</option>
            <option value="Saint Helena">Saint Helena</option>
            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
            <option value="Saint Lucia">Saint Lucia</option>
            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
            <option value="Samoa">Samoa</option>
            <option value="San Marino">San Marino</option>
            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
            <option value="Saudi Arabia">Saudi Arabia</option>
            <option value="Senegal">Senegal</option>
            <option value="Serbia">Serbia</option>
            <option value="Seychelles">Seychelles</option>
            <option value="Sierra Leone">Sierra Leone</option>
            <option value="Singapore">Singapore</option>
            <option value="Slovakia">Slovakia</option>
            <option value="Slovenia">Slovenia</option>
            <option value="Solomon Islands">Solomon Islands</option>
            <option value="Somalia">Somalia</option>
            <option value="South Africa">South Africa</option>
            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
            <option value="Spain">Spain</option>
            <option value="Sri Lanka">Sri Lanka</option>
            <option value="Sudan">Sudan</option>
            <option value="Suriname">Suriname</option>
            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
            <option value="Swaziland">Swaziland</option>
            <option value="Sweden">Sweden</option>
            <option value="Switzerland">Switzerland</option>
            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
            <option value="Taiwan">Taiwan</option>
            <option value="Tajikistan">Tajikistan</option>
            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
            <option value="Thailand">Thailand</option>
            <option value="Timor-leste">Timor-leste</option>
            <option value="Togo">Togo</option>
            <option value="Tokelau">Tokelau</option>
            <option value="Tonga">Tonga</option>
            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
            <option value="Tunisia">Tunisia</option>
            <option value="Turkey">Turkey</option>
            <option value="Turkmenistan">Turkmenistan</option>
            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
            <option value="Tuvalu">Tuvalu</option>
            <option value="Uganda">Uganda</option>
            <option value="Ukraine">Ukraine</option>
            <option value="United Arab Emirates">United Arab Emirates</option>
            <option value="United Kingdom">United Kingdom</option>
            <option value="United States">United States</option>
            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
            <option value="Uruguay">Uruguay</option>
            <option value="Uzbekistan">Uzbekistan</option>
            <option value="Vanuatu">Vanuatu</option>
            <option value="Venezuela">Venezuela</option>
            <option value="Viet Nam">Viet Nam</option>
            <option value="Virgin Islands, British">Virgin Islands, British</option>
            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
            <option value="Wallis and Futuna">Wallis and Futuna</option>
            <option value="Western Sahara">Western Sahara</option>
            <option value="Yemen">Yemen</option>
            <option value="Zambia">Zambia</option>
            <option value="Zimbabwe">Zimbabwe</option>
          </select>
            <img src="img/down-arrow.svg" class="country_img"/>
        </div>
      </div>
      <div class="form_item_group form_item_group_mobile">
        <div class="form_item_group_column">
          <div class="form_item">
            <label for="profession" class="label_text">Profesia: <span style="color: darkred;">&nbsp*</span></label>
            <input name="profession" type="text" class="input_text valid_text">
          </div>
          <div class="form_item">
            <label for="department" class="label_text">Inštitúcia a oddelenie: <span style="color: darkred;">&nbsp*</span></label>
            <input name="department" type="text" class="input_text valid_text">
          </div>
          <div class="form_item" style="margin: 15px 0 10px 0;">
            <label for="request" class="label_text" >Request:<span style="color: darkred;">&nbsp*</span></label>
              <textarea name="request" type="text" class="input_text valid_text" style="height: 150px; padding: 20px 20px;"></textarea>
          </div>
            <div class="form_item_group">
                <input name="privacypolicy" type="checkbox" class="valid_checkbox">
                <label for="privacypolicy">Prečítal som si Zásady ochrany osobných údajov a súhlasím s nimi.<span style="color: darkred;">&nbsp*</span></label>
            </div>
        </div>

        <div class="form_item_group_column valid_about_container" style="margin-top: 30px;">
          <div class="form_checkbox_group">
            <label style="margin: 10px 0;"><b>Ako ste sa dozvedeli o Human Anatomy VR?</b><span style="color: darkred;">&nbsp*</span></label>
            <div class="form_item_group">
              <input name="onlinestore" type="checkbox" class="valid_checkbox_about">
              <label for="onlinestore">z internetového obchodu</label>
            </div>
            <div class="form_item_group">
                <input name="searchingonline" type="checkbox" class="valid_checkbox_about">
                <label for="searchingonline">vyhľadávaním na internete</label>
              </div>
            <div class="form_item_group">
            <input name="socialmedia" type="checkbox" class="valid_checkbox_about">
            <label for="socialmedia">zo sociálnych médií</label>
          </div>
            <div class="form_item_group">
                <input name="other" id="otherchecker" type="checkbox" class="valid_checkbox_about">
                <input name="otherinput" type="text" class="valid_text_about" id="otherinput" placeholder="Iné" style="transform: translateY(-3px); border: 1px solid #A9A9A9; padding-left: 10px;">
              </div>
          </div>
        </div>
      </div>
        <div class="form_submit_container">
            <h5>ĎAKUJEME VÁM, TÍM VIRTUAL MEDICINE.</h5>
            <button id="submit_button" class="try">ODOSLAŤ</button>
        </div>
    </form>
  </section>
  <div class="address">
      <div>
          <b>Virtual Medicine, s.r.o.</b><br>
          Digital Park II, Einsteinova 25<br>
          851 01 Bratislava, Slovakia<br>
          European Union<br>
      </div>
      <div>
        <b>Representation in the USA</b><br>
        Virtualist, LLC<br>
        Zoltan Gombos, CEO<br>
        <a href="mailto:virtualist@myvirtualist.com"><u>virtualist@myvirtualist.com</u></a><br>
      </div>
  </div>

    <hr style="opacity: 0.5; margin-top: 30px;">

  <section class="scroll_up">
    <button class="try" onclick="$('html, body').animate({scrollTop : 0}, 1000);">SCROLL UP</button>
  </section>

  <footer>
    <div class="footer_menu_container">
      <div class="def_list_group">
        <dl>
          <dt>Virtual Medicine</dt>
          <dd><a href="xrexperience.html#try_for_free">Try for free</a></dd>
          <dd><a href="xrexperience.html">Buy now</a></dd>
        </dl>
        <dl>
          <dt>Products</dt>
          <dd><a href="xrexperience.html#download_now">Anatomy Explorer</a></dd>
          <dd><a href="xrexperience.html#try_for_free">AR Anatomy</a></dd>
          <dd><a href="institution.html">Virtual Anatomy<br>Classroom</a></dd>
          <dd><a href="xrexperience.html#try_for_free">High School Anatomy</a></dd>
        </dl>
        <dl>
          <dt>License plans</dt>
          <dd><a href="student.html">Student</a></dd>
          <dd><a href="professional.html">Professional</a></dd>
          <dd><a href="institution.html">Institution</a></dd>
        </dl>
        <dl>
          <dt>Company</dt>
          <dd><a href="contact.html">About us</a></dd>
          <dd><a href="contact-form.php">Contact us</a></dd>
          <dd><a href="contact.html#team">Team</a></dd>
        </dl>
      </div>
    </div>
    <div class="footer_menu_logo">
      <img src="img/nav-logo.png" onclick="location.href='index.html';" style="cursor: pointer;">
      <span style="font-size: 11px; margin: 10px 0;">Copyright © 2022, Virtual Medicine | <span onclick="window.open('doc/License_VM.pdf')" style="cursor: pointer; color: #8EC1FE;">License</span></span>
    </div>
  </footer>

  <script src="js/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="tilt.js/src/tilt.jquery.js"></script>
  <script src="js/vendor/modernizr-3.11.2.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>
